import MySQLdb


RMP = 'D:\\Work\\auth\\.rmpsql'
con = MySQLdb.connect(read_default_file=RMP, db='business')
cur = con.cursor()
q = """
SELECT b.name0,
	CAST(c.id0 AS CHAR(255)) AS rmp_pub_id,
    c.customid0 AS publisher,
	c.fullname0 AS publisher_name,
    CAST(a.id0 AS CHAR(255)) AS rmp_plac_id,
    a.customid0 AS placement,
    a.title0 AS placement_name,
    a.display_price_floor,
    a.video_price_floor
FROM client0 AS c
JOIN ssp0 AS b ON b.zone0 = c.zone0 AND c.ssp = b.id0
JOIN adspot0 AS a ON a.owner0 = c.id0 AND c.mainservid0 = 3
WHERE a.archived0 = 'N'
	AND c.archived0 = 'N'
"""
cur.execute(q)
data = cur.fetchall()
con.close()


DB = '/home/cbeecher/.auth/.my.conf'
DB = 'D:\\Work\\auth\\.my.conf'
con = MySQLdb.connect(read_default_file=DB, db='rmp')
cur = con.cursor()
cur.execute("TRUNCATE TABLE ssp_pub")
con.commit()
cur.executemany("""
INSERT INTO ssp_pub (ssp, rmp_pub_id, publisher, publisher_name, rmp_plac_id, placement, placement_name, display_price_floor, video_price_floor)
VALUES
(%s, %s, %s, %s, %s, %s, %s, %s, %s)
""", data)
con.commit()
con.close()
